#ifndef __DOTS_H
#define __DOTS_H

#include <stdint.h>

uint32_t get_returnAddr(void);
void* get_ebp_saved(void);
void* get_ebp_current(void);

#endif
