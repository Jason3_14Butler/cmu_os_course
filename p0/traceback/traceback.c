/** @file traceback.c
 *  @brief The traceback function
 *
 *  This file contains the traceback function for the traceback library
 *
 *  @author Harry Q. Bovik (hqbovik)
 *  @author Jason Butler
 *  @bug Unimplemented
 */
#include <string.h>
#include <dotS.h>
#include <unistd.h>

#include "traceback_internal.h"

extern char etext, edata, end;

int char_size;

functsym_t functions_l[FUNCTS_MAX_NUM];

static FILE *tracebackOutFile;

int check_pointer(void* local_ebp, void* star_current_ebp, const int offset);
int check_string(void* local_ebp, void* star_current_ebp, const char* check_str);
int check_data_segment( void* local_ebp, void* star_current_ebp, const int offset);
int check_stack(void* local_ebp, void* star_current_ebp, const int offset);
int check_datasegment_string(void* local_ebp, void* star_current_ebp, const char* check_str);
int check_stack_string(void* local_ebp, void* star_current_ebp, const char* check_str);

#define traceback_out(...) fprintf(tracebackOutFile,__VA_ARGS__)

/* Given the return address for a function from the stack
   calculate the address of the calling function in the test segment */
inline void* get_func_addr_from_ret_addr(uint32_t retAddr)
{
    uint32_t call_address = retAddr -4; //address of the call instruction
    uint32_t call_operand = *(uint32_t*)call_address;
    /*the address of the function is the address of the
      call instruction plus the operand to the call instruction */
    return (void*)(retAddr + call_operand);
}

int get_func_index_from_address(void* func_address)
{
    int i = 0;
    for(i=0; i<FUNCTS_MAX_NUM; i++)
    {
        if(func_address == functions[i].addr)
            return i;
    }

    return FUNCTS_MAX_NUM;
}

/* given a function name in string form find the index
   for the function in the functions array created
   by the python elf tools  */
int get_func_index_from_name(char* func_name)
{
    int i = 0;
    for(i=0; i<FUNCTS_MAX_NUM; i++)
    {
        if(0==strncmp(functions[i].name,func_name,FUNCTS_MAX_NAME))
        {
            return i;
        }
    }
    return FUNCTS_MAX_NUM;
}

/* returns the number of strings in a NULL terminated
   string array CMU specific             */
inline int get_str_array_len(char ** str_array)
{
    int index = 0;
    while((NULL != str_array[index]) && (index < 4))
    {
        index++;
    }

    return index;
}

inline int check_stack(void* local_ebp, void* star_current_ebp, const int offset)
{
    if(( *(char**)((char*)local_ebp+offset) < (char*)local_ebp) || (*(char**)((char*)local_ebp+offset)  > (char*)star_current_ebp)) {
        return 0;
    }
    else
        return 1;
}

inline int check_data_segment( void* local_ebp, void* star_current_ebp, const int offset)
{
    if((*(char**)((char*)local_ebp+offset) < &etext) || (*(char**)((char*)local_ebp+offset) > (char*)sbrk(0)))
    {
        return 0;
    }
    else
        return 1;
}

/* given a pointer to a string check to make sure the string is within the current stack frame */
inline int check_pointer(void* local_ebp, void* star_current_ebp, const int offset)
{
    if(!check_data_segment(local_ebp, star_current_ebp, offset) && !check_stack(local_ebp,star_current_ebp, offset)) {
        return 0;
    }
    else
        return 1;
}

/* given a pointer to a string check to make sure the string is within the current stack frame */
inline int check_string(void* local_ebp, void* star_current_ebp, const char* check_str)
{
    if(!check_stack_string(local_ebp,star_current_ebp, check_str) && !check_datasegment_string(local_ebp,star_current_ebp,check_str)) {
        return 0;
    }
    else
        return 1;
}

inline int check_stack_string(void* local_ebp, void* star_current_ebp, const char* check_str)
{
    if((check_str < (char*)local_ebp) || (check_str > (char*)star_current_ebp)) {
        return 0;
    }
    else
        return 1;
}

inline int check_datasegment_string(void* local_ebp, void* star_current_ebp, const char* check_str)
{
    if((check_str < &etext) || (check_str > &end))
    {
        return 0;
    }
    else
        return 1;
}

void print_str_array(const char *var_name, char ** my_str_array, void* local_ebp, void* star_current_ebp)
{
    int index = 0;
    int str_len = get_str_array_len(my_str_array);

    if(str_len == 0)
    {
        traceback_out("string array %s=%p", var_name,(void*)my_str_array);
    }
    else
    {
        traceback_out("string array %s={\"",var_name);
        for(index = 0; index < str_len; index++)
        {
            if(check_string(local_ebp, star_current_ebp, my_str_array[index]))
            {
                if(strnlen(my_str_array[index], 26) > 25)
                {
                    traceback_out("\"%.*s\"...", 25, my_str_array[index]);
                }
                else
                {
                    traceback_out("\"%.*s\"", 25, my_str_array[index]);
                }
            }
            else
            {
                traceback_out("UNKNOWN %p", my_str_array[index]);
            }
            if(index < (str_len -1))
                traceback_out(",");
            if(index == 3)
                break;
        }

        if(str_len > 3)
            traceback_out("...");
        traceback_out("}");
    }
}

void print_var(void* local_ebp, void* star_current_ebp, const argsym_t* arg_ptr)
{
    switch(arg_ptr->type) {
    case TYPE_CHAR:
        traceback_out("char %s=\'%c\'", arg_ptr->name, *(char*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_INT:
        traceback_out("int %s=%i", arg_ptr->name, *(int*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_FLOAT:
        traceback_out("float %s=%f", arg_ptr->name, *(float*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_DOUBLE:
        traceback_out("double %s=%fl", arg_ptr->name, *(double*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_STRING:
        /* check to make sure strings are within current stack frame */
        if(check_pointer(local_ebp, star_current_ebp, arg_ptr->offset))
        {
            if(strnlen(*(char **)(local_ebp+arg_ptr->offset), 26) > 25)
            {
                traceback_out("char *%s=\"%.*s\"...", arg_ptr->name, 25, *(char **)(local_ebp+arg_ptr->offset));
            }
            else
            {
                traceback_out("char *%s=\"%.*s\"", arg_ptr->name, 25, *(char **)(local_ebp+arg_ptr->offset));
            }
        }
        else
            traceback_out("UNKNOWN %s=%p", arg_ptr->name, (void*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_VOIDSTAR:
        traceback_out("void* %s=0v%08x", arg_ptr->name, *(uint32_t*)(local_ebp+arg_ptr->offset));
        break;
    case TYPE_STRING_ARRAY:
        print_str_array(arg_ptr->name, *(char***)(local_ebp+arg_ptr->offset),local_ebp, star_current_ebp);
        break;
    case TYPE_UNKNOWN:
        traceback_out("UNKNOWN %s=%p", arg_ptr->name, (void*)(local_ebp+arg_ptr->offset));
        break;
    }
}

void traceback(FILE *fp)
{
    tracebackOutFile = fp;
    char_size = sizeof(char);
    void* addr = sbrk(0);
    *(char*)(addr-1) = 0;

    void *saved_ebp = get_ebp_saved();
    int function_index = FUNCTS_MAX_NUM;
    int args_index = ARGS_MAX_NUM;
    void* function_address = NULL;
    while(saved_ebp != NULL) {
        function_address = get_func_addr_from_ret_addr(*(uint32_t*)((uint32_t)saved_ebp + 4));
        function_index = get_func_index_from_address(function_address);
        if(FUNCTS_MAX_NUM != function_index)
        {
            traceback_out("Function %s(", functions[function_index].name);
            args_index = 0;
            while(NULL == memchr(functions[function_index].args[args_index].name,'\000',1))
            {
                if(args_index != 0)
                    traceback_out(", ");
                print_var((void*)saved_ebp, (void*)*(uint32_t*)saved_ebp, &functions[function_index].args[args_index]);
                args_index++;
            }
            traceback_out("),\n");
        }
        saved_ebp = (void*)(*(uint32_t*)saved_ebp);
    }
    function_index = get_func_index_from_name("main");
    traceback_out("main %p\n", functions[function_index].addr);


}


